/**
 * Created by jk_87 on 1/4/2016.
 */
//load express
var express = require("express");
//create express app
var app = express();
//specify docu root
app.use(
    express.static(__dirname + "/public")
);
//listen to port 3000
app.listen(3000, function () {
    console.info("App starting on port 3000");
    console.info("Ctrl-c to terminiate")
    });